// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'

Vue.config.productionTip = false
import Axios from 'axios';



Axios.defaults.baseURL = `${process.env.server_env}${process.env.base_url}`;
Axios.defaults.headers.common.Accept = 'application/json';
Vue.$http = Axios;
Object.defineProperty(Vue.prototype, '$http', {
	get() {
		return Axios;
	},
});





import VeeValidate from 'vee-validate';

Vue.use(VeeValidate);


import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en'

Vue.use(ElementUI, { locale });

require('./assets/sass/app.scss');

new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
