import Vue from 'vue';

export default{
    change_step(state, route_name) {
		window.scrollTo(0, 0);
		state.steps[route_name].visited = true;
		Vue.set(state,'currentStep',route_name);
	},
}