import Vue from 'vue';
import Vuex from 'vuex';
import state from './state';
import mutations from './mutations';

Vue.use(Vuex);
export default new Vuex.Store({
    state,
    mutations,
    getters:{
     // Compute derived state based on the current state. More like computed property.
    },

    actions:{
     // Get data from server and send that to mutations to mutate the current state
    }
   })
   // You can assign a store to variable and export 
